# LKCAMP Kernel Day

Repository of LKCAMP's first Kernel Day contribution

Email list: <https://lists.sr.ht/~jn5/LKCAMP-H2HC>  
Mirror repository: <https://gitlab.com/jn513/kernel-day>  
Presentation link: <https://docs.google.com/presentation/d/19IVdCEquxaV9m9P3XjghC2AeUrZ5hd0W_PNdyeVpVHA/edit?usp=sharing>

Gitlab site: https://kernel-day-jn513-ae3fe0951487f3b89604c2c131d40494b121f9ac215697.gitlab.io/

# Tutorial

## Install dependencies

Debian/Ubuntu:

```bash
 sudo apt install git git-email hugo
```

Arch:

```bash
sudo pacman -S git perl-authen-sasl perl-io-socket-ssl perl-email-mime hugo
```

Fedora:

```bash
sudo dnf install git hugo
```

## clone repo

```
git clone --recurse-submodules https://gitlab.com/jn513/kernel-day.git
cd kernel-day/
```

Configure git to work with your favorite editor(nano ?)

```
 git config --global core.editor <your favorite editor>
```

Add git config

```bash
nano .git/config
```

add the following lines to the end of the file:

```
[user]
    name = <your name>
    email =<your email
```

see the branchs

```css
git branch -a
```

## Configure git send-email

Go to gmail -> settings -> See all settings -> Forwarding and POP/IMAP

Enable POP and IMAP

Activate 2FA. Visit [Link](https://security.google.com/settings/security/apppasswords)

Follow this tutorial to generate an APP Password [tutorial](https://support.google.com/accounts/answer/185833?hl=pt-BR&sjid=844001680102619355-SA)

!! SAVE THE APP PASSWORD!!

### Add configurations

Open the configuration file

```bash
nano .git/config
```

Add the settings below at the end of the file

```
[sendemail]
    smtpserver = smtp.gmail.com
    smtpuser = <your email>
    smtppass = <you app password>
    smtpserverport = 587
    smtpencryption = tls
```

## Adding your contribution

Create a new branch to patch in the right way:

```
git checkout -b first-patch
```

Create new post

```bash
hugo new content posts/<post name>.md
```

_I recommend that the post name starts with your name and continues with something like "my first contribution". Ex: julio-nunes-my-first-contribution.md_

After creating the post, simply create the file created with your favorite text editor.

Test your contribution:

```bash
hugo server
```

_The "hugo server" command will start a web server. If everything goes as expected, it will be possible to access the web page through a URL provided by Hugo himself. By default, Hugo operates on port 1313. So just type "[localhost:1313](http://localhost:1313)" in your browser to open the page. If the process went smoothly, you will see your post on the page._

add the changes

```
git add <file modified or added>
```

commit

```
git commit -s -v
```

_The -v flag shows the diff of what you're committing. the flag -s will add the Signed-off-by line_

When opening the editor, the commit message can be inserted from the first line. For this project I recommend messages follow the following pattern.

```bash
Adding my first contribution

Creating post with the first contribution by Julio Nunes Avelar. The name of the file with the post is: julio-nunes-first-contribution.md
```

Generate the patch file

```
git format-patch -o /tmp/ HEAD^
```

#### Test It:

You can either send it directly:

```
git send-email --annotate --suppress-cc=all HEAD^
```

or pass the patch file that was generated:

```
git send-email --to "~jn5/LKCAMP-H2HC@lists.sr.ht"  <patch file address>
```

### Working with the real thing

When you work with the kernel project, there will be a few other steps between adding your changes and sending your patch.

The kernel follows a rigorous code style, to verify if your patch follows all the rules you should always run the [checkpatch.pl]() script.

To findout whats is the address of the mantainers you can run the following command:

```
perl scripts/get_maintainer.pl --separator , --nokeywords --nogit --nogit-fallback --norolestats -f <file_path>
```

For more detailed information about the kernel contribution process consult the [kernel newbies tutorial]()
